"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.keys = exports.Client = exports.Keys = void 0;
var Keys = require("./keys");
exports.Keys = Keys;
var client_1 = require("./client");
exports.default = client_1.RokuClient;
__exportStar(require("./client"), exports);
__exportStar(require("./discover"), exports);
/**
 * Import `RokuClient` instead.
 * @deprecated
 */
var Client = client_1.RokuClient;
exports.Client = Client;
/**
 * Import `Keys` instead.
 * @deprecated
 */
var keys = Keys;
exports.keys = keys;
//# sourceMappingURL=index.js.map