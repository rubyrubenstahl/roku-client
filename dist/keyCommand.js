"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getCommand = void 0;
function getCommand(key) {
    if (typeof key === 'string') {
        return key;
    }
    return key.command;
}
exports.getCommand = getCommand;
//# sourceMappingURL=keyCommand.js.map