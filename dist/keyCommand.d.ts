declare type Keys = typeof import('./keys');
export interface KeyCommand {
    command: string;
    name: string;
}
declare type KeyNameInterface<T extends Record<string, KeyCommand>> = {
    [N in T[keyof T]['command']]: any;
};
export declare type KeyName = keyof KeyNameInterface<Keys>;
export declare type KeyType = KeyCommand | string;
export declare function getCommand(key: KeyType): string;
export {};
