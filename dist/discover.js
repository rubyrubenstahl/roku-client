"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.discoverAll = exports.discover = void 0;
var url = require("url");
var events_1 = require("events");
var node_ssdp_1 = require("node-ssdp");
var _debug = require("debug");
var debug = _debug('roku-client:discover');
var DEFAULT_TIMEOUT = 10000;
function parseAddress(location) {
    var parts = url.parse(location);
    parts.path = null;
    parts.pathname = null;
    return url.format(parts);
}
/**
 * Helper class abstracting the lifecycle of locating Roku devices on the
 * network.
 */
var RokuFinder = /** @class */ (function (_super) {
    __extends(RokuFinder, _super);
    function RokuFinder() {
        var _this = _super.call(this) || this;
        _this.intervalId = null;
        _this.timeoutId = null;
        _this.running = false;
        _this.client = new node_ssdp_1.Client();
        _this.client.on('response', function (headers) {
            if (!_this.running) {
                return;
            }
            var SERVER = headers.SERVER, LOCATION = headers.LOCATION;
            if (SERVER && LOCATION && SERVER.includes('Roku')) {
                var address = parseAddress(LOCATION);
                _this.emit('found', address);
            }
        });
        return _this;
    }
    RokuFinder.prototype.start = function (timeout) {
        var _this = this;
        debug('beginning search for roku devices');
        this.running = true;
        var search = function () {
            _this.client.search('roku:ecp');
        };
        var done = function () {
            _this.stop();
            _this.emit('timeout');
        };
        search();
        this.intervalId = setInterval(search, 1000);
        this.timeoutId = setTimeout(done, timeout);
    };
    RokuFinder.prototype.stop = function () {
        clearInterval(this.intervalId);
        clearTimeout(this.timeoutId);
        this.running = false;
        this.client.stop();
    };
    return RokuFinder;
}(events_1.EventEmitter));
/**
 * Discover one Roku device on the network. Resolves to the first Roku device
 * that responds to the ssdp request.
 * @param timeout The time to wait in ms before giving up.
 * @return A promise resolving to a Roku device's address.
 */
function discover(timeout) {
    if (timeout === void 0) { timeout = DEFAULT_TIMEOUT; }
    return new Promise(function (resolve, reject) {
        var finder = new RokuFinder();
        var startTime = Date.now();
        function elapsedTime() {
            return Date.now() - startTime;
        }
        finder.on('found', function (address) {
            finder.stop();
            resolve(address);
            debug("found Roku device at " + address + " after " + elapsedTime() + "ms");
        });
        finder.on('timeout', function () {
            reject(new Error("Could not find any Roku devices after " + timeout / 1000 + " seconds"));
        });
        finder.start(timeout);
    });
}
exports.discover = discover;
/**
 * Discover all Roku devices on the network. This method always waits the full
 * timeout, resolving to a list of all Roku device addresses that responded
 * within `timeout` ms.
 * @param timeout The time to wait in ms before giving up.
 * @return A promise resolving to a list of Roku device addresses.
 */
function discoverAll(timeout) {
    if (timeout === void 0) { timeout = DEFAULT_TIMEOUT; }
    return new Promise(function (resolve, reject) {
        var finder = new RokuFinder();
        var addresses = [];
        var startTime = Date.now();
        function elapsedTime() {
            return Date.now() - startTime;
        }
        finder.on('found', function (address) {
            if (!addresses.includes(address)) {
                debug("found Roku device at " + address + " after " + elapsedTime() + "ms");
                addresses.push(address);
            }
        });
        finder.on('timeout', function () {
            if (addresses.length > 0) {
                debug('found Roku devices at %o after %dms', addresses, elapsedTime());
                resolve(addresses);
            }
            else {
                reject(new Error("Could not find any Roku devices after " + timeout / 1000 + " seconds"));
            }
        });
        finder.start(timeout);
    });
}
exports.discoverAll = discoverAll;
//# sourceMappingURL=discover.js.map