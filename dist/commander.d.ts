import * as Keys from './keys';
import { KeyCommand, KeyName } from './keyCommand';
import type { RokuClient } from './client';
declare type ClientInterface<T extends Record<string, KeyCommand>> = {
    [N in T[keyof T]['name']]: (count?: number) => Commander;
};
export interface Commander extends ClientInterface<typeof Keys> {
}
export declare class Commander {
    private readonly client;
    private commands;
    constructor(client: RokuClient);
    /**
     * Send the given string to the Roku device.
     * @param text The string to send.
     * @return This `Commander` instance for chaining.
     */
    text(text: string): Commander;
    /**
     * Press the given key `count` times. Useful if the keys to press
     * are unknown at coding time. Each key is also available as a
     * camelcase method for convenience.
     * @param key The key to press, from the `keys` module.
     * @param count The number of times to press the key.
     */
    keypress(key: KeyName | KeyCommand, count?: number): Commander;
    /**
     * A convenience method for running commands conditionally. The callback
     * function will be called with the instance of the commander, and will return
     * the current commander instance. This makes it easy to run conditional logic
     * within the chain of commands.
     * @param fn A callback function that accepts the `Commander` instance. It may
     *     call any `Commander` method, or do nothing at all.
     * @return This `Commander` instance for chaining.
     *
     * @example
     * command
     *   .exec(cmd => goUp ? cmd.up(10) : cmd.down(10))
     *   .right()
     *   .select()
     *   .send();
     */
    exec(fn: (commander: Commander) => Commander | null | undefined): Commander;
    /**
     * Wait the given `timeout` time before sending the next command.
     * @param timeout The number of millis to wait.
     * @return This `Commander` instance for chaining.
     */
    wait(timeout: number): Commander;
    /**
     * Send all of the configured commands to the Roku.
     */
    send(): Promise<void>;
    private runCommand;
}
export {};
