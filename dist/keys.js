"use strict";
/**
 * Create a mapping of keys to make them easier to remember.
 * @see https://sdkdocs.roku.com/display/sdkdoc/External+Control+Guide#ExternalControlGuide-KeypressKeyValues
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.POWER = exports.INPUT_AV1 = exports.INPUT_HDMI4 = exports.INPUT_HDMI3 = exports.INPUT_HDMI2 = exports.INPUT_HDMI1 = exports.INPUT_TUNER = exports.CHANNEL_DOWN = exports.CHANNEL_UP = exports.VOLUME_MUTE = exports.VOLUME_UP = exports.VOLUME_DOWN = exports.FIND_REMOTE = exports.ENTER = exports.SEARCH = exports.BACKSPACE = exports.OPTIONS = exports.STAR = exports.INFO = exports.INSTANT_REPLAY = exports.BACK = exports.UP = exports.DOWN = exports.RIGHT = exports.LEFT = exports.SELECT = exports.PLAY = exports.FORWARD = exports.FWD = exports.REVERSE = exports.REV = exports.HOME = void 0;
function key(command, name) {
    return { command: command, name: name };
}
// Standard Keys
exports.HOME = key('Home', 'home');
exports.REV = key('Rev', 'reverse');
exports.REVERSE = exports.REV;
exports.FWD = key('Fwd', 'forward');
exports.FORWARD = exports.FWD;
exports.PLAY = key('Play', 'play');
exports.SELECT = key('Select', 'select');
exports.LEFT = key('Left', 'left');
exports.RIGHT = key('Right', 'right');
exports.DOWN = key('Down', 'down');
exports.UP = key('Up', 'up');
exports.BACK = key('Back', 'back');
exports.INSTANT_REPLAY = key('InstantReplay', 'instantReplay');
exports.INFO = key('Info', 'info');
exports.STAR = key('Info', 'star');
exports.OPTIONS = key('Info', 'options');
exports.BACKSPACE = key('Backspace', 'backspace');
exports.SEARCH = key('Search', 'search');
exports.ENTER = key('Enter', 'enter');
// For devices that support "Find Remote"
exports.FIND_REMOTE = key('FindRemote', 'findRemote');
// For Roku TV
exports.VOLUME_DOWN = key('VolumeDown', 'volumeDown');
exports.VOLUME_UP = key('VolumeUp', 'volumeUp');
exports.VOLUME_MUTE = key('VolumeMute', 'volumeMute');
// For Roku TV while on TV tuner channel
exports.CHANNEL_UP = key('ChannelUp', 'channelUp');
exports.CHANNEL_DOWN = key('ChannelDown', 'channelDown');
// For Roku TV current input
exports.INPUT_TUNER = key('InputTuner', 'inputTuner');
exports.INPUT_HDMI1 = key('InputHDMI1', 'inputHDMI1');
exports.INPUT_HDMI2 = key('InputHDMI2', 'inputHDMI2');
exports.INPUT_HDMI3 = key('InputHDMI3', 'inputHDMI3');
exports.INPUT_HDMI4 = key('InputHDMI4', 'inputHDMI4');
exports.INPUT_AV1 = key('InputAV1', 'inputAV1');
// For devices that support being turned on/off
exports.POWER = key('Power', 'power');
//# sourceMappingURL=keys.js.map