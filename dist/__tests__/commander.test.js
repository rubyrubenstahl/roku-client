"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var client_1 = require("../client");
var commander_1 = require("../commander");
var Keys = require("../keys");
describe('Commander', function () {
    var methods;
    var client;
    var commander;
    beforeEach(function () {
        methods = [];
        client = new client_1.RokuClient('address');
        client.keypress = function (command) {
            methods.push(command);
            return Promise.resolve();
        };
        commander = new commander_1.Commander(client);
    });
    it('should allow chaining methods', function () {
        return commander
            .up(2)
            .down(2)
            .left()
            .right()
            .left()
            .right()
            .text('b')
            .text('a')
            .enter()
            .send()
            .then(function () {
            expect(methods).toEqual([
                'Up',
                'Up',
                'Down',
                'Down',
                'Left',
                'Right',
                'Left',
                'Right',
                'b',
                'a',
                'Enter',
            ]);
        });
    });
    it('should allow for key strings to be used', function () {
        return commander
            .keypress(Keys.VOLUME_DOWN)
            .keypress(Keys.VOLUME_UP)
            .keypress(Keys.VOLUME_MUTE)
            .keypress('Power')
            .send()
            .then(function () {
            expect(methods).toEqual([
                'VolumeDown',
                'VolumeUp',
                'VolumeMute',
                'Power',
            ]);
        });
    });
    it('should allow commands to be added in the exec method', function () {
        return commander
            .exec(function (cmd) { return cmd.down(); })
            .up()
            .send()
            .then(function () {
            expect(methods).toEqual(['Down', 'Up']);
        });
    });
    it('should allow waiting between commands', function () {
        jest.useFakeTimers();
        var cmd = commander.wait(2000).up().send();
        return new Promise(function (resolve) {
            setImmediate(function () {
                expect(methods.length).toBe(0);
                jest.runAllTimers();
                resolve(cmd.then(function () {
                    expect(methods).toEqual(['Up']);
                }));
            });
        });
    });
    it('should allow sending more than once', function () { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    commander.up(2).down(2).left().right();
                    return [4 /*yield*/, commander.send()];
                case 1:
                    _a.sent();
                    expect(methods).toEqual(['Up', 'Up', 'Down', 'Down', 'Left', 'Right']);
                    methods = [];
                    return [4 /*yield*/, commander.send()];
                case 2:
                    _a.sent();
                    expect(methods).toEqual(['Up', 'Up', 'Down', 'Down', 'Left', 'Right']);
                    return [2 /*return*/];
            }
        });
    }); });
});
//# sourceMappingURL=commander.test.js.map