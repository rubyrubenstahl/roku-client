"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var path = require("path");
var stream = require("stream");
var client_1 = require("../client");
var fetchPonyfill = require("fetch-ponyfill");
var fetchObjects = fetchPonyfill();
var fetch = fetchObjects.fetch;
var clientAddr = 'http://192.168.1.61:8060';
function loadResponse(name, asBuffer) {
    if (asBuffer === void 0) { asBuffer = false; }
    var file = name.includes('.') ? name : name + ".xml";
    var data = fs.readFileSync(path.join(__dirname, 'assets', file));
    if (!asBuffer) {
        return data.toString('utf-8');
    }
    var bufferStream = new stream.PassThrough();
    bufferStream.end(data);
    return bufferStream;
}
describe('Client', function () {
    var client;
    beforeEach(function () {
        client = new client_1.RokuClient(clientAddr);
        fetch.mockClear();
    });
    describe('#discover()', function () {
        it('should resolve to a client instance for the first address found', function () {
            require('node-ssdp').__setHeaders({
                SERVER: 'Roku UPnP/1.0 MiniUPnPd/1.4',
                LOCATION: 'http://192.168.1.17:8060/dial/dd.xml',
            });
            return client_1.RokuClient.discover().then(function (c) {
                expect(c).toBeInstanceOf(client_1.RokuClient);
                expect(c.ip).toEqual('http://192.168.1.17:8060');
            });
        });
    });
    describe('#constructor()', function () {
        it('should construct a new Client object', function () {
            expect(client).toBeDefined();
            expect(client.ip).toEqual(clientAddr);
        });
        it('should add missing http://', function () {
            var c = new client_1.RokuClient('192.168.1.2:1111');
            expect(c.ip).toEqual('http://192.168.1.2:1111');
        });
        it('should add the default port if omitted', function () {
            var c = new client_1.RokuClient('192.168.1.2');
            expect(c.ip).toEqual("http://192.168.1.2:" + client_1.ROKU_DEFAULT_PORT);
        });
    });
    describe('#apps()', function () {
        it('should return a list of apps', function () {
            fetch.mockResponse(loadResponse('apps'));
            return client.apps().then(function (apps) {
                expect(apps).toBeInstanceOf(Array);
                apps.forEach(function (app) {
                    expect(app).toEqual(expect.objectContaining({
                        id: expect.any(String),
                        name: expect.any(String),
                        type: expect.any(String),
                        version: expect.any(String),
                    }));
                });
            });
        });
    });
    describe('#active()', function () {
        it('should return the active app', function () {
            fetch.mockResponse(loadResponse('active-app'));
            return client.active().then(function (app) {
                expect(app).toEqual(expect.objectContaining({
                    id: expect.any(String),
                    name: expect.any(String),
                    type: expect.any(String),
                    version: expect.any(String),
                }));
            });
        });
        it('should return null if there is not an active app', function () {
            fetch.mockResponse(loadResponse('active-app-none'));
            return client.active().then(function (app) {
                expect(app).toBeNull();
            });
        });
        it('should reject if multiple apps are returned', function () {
            fetch.mockResponse(loadResponse('active-multiple'));
            return client
                .active()
                .then(function () {
                throw new Error('Should have thrown');
            })
                .catch(function (err) {
                expect(err).toBeDefined();
                expect(err).toBeInstanceOf(Error);
            });
        });
    });
    describe('#info()', function () {
        it('should return info for the roku device', function () {
            fetch.mockResponse(loadResponse('info'));
            return client.info().then(function (info) {
                expect(info).toBeInstanceOf(Object);
                expect(Object.keys(info).length).toEqual(29);
                expect(info['model-name']).toBeUndefined();
                expect(info.modelName).toEqual('Roku 3');
            });
        });
    });
    describe('#mediaPlayer()', function () {
        it('should return info for the media player', function () {
            fetch.mockResponse(loadResponse('media-player'));
            return client.mediaPlayer().then(function (info) {
                expect(info).toBeInstanceOf(Object);
                expect(info['state']).toEqual('play');
                expect(info['position']).toEqual('6916 ms');
                expect(info.app['id']).toEqual('dev');
                expect(info.app['name']).toEqual('MultiLive');
                expect(info.format['audio']).toEqual('aac');
                expect(info.format['captions']).toEqual('none');
                expect(info.format['video']).toEqual('mpeg4_15');
                expect(info.format['drm']).toEqual('none');
                expect(info['isLive']).toEqual('false');
            });
        });
    });
    describe('#keypress()', function () {
        it('should press the home button', function () {
            return client.keypress('Home').then(function () {
                expect(fetch).toHaveBeenCalledWith(clientAddr + "/keypress/Home", {
                    method: 'POST',
                });
            });
        });
        it('should send a Lit_ command if a single character is passed in', function () { return __awaiter(void 0, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, client.keypress('a')];
                    case 1:
                        _a.sent();
                        expect(fetch).toHaveBeenCalledWith(clientAddr + "/keypress/Lit_a", {
                            method: 'POST',
                        });
                        return [2 /*return*/];
                }
            });
        }); });
        it('should url encode Lit_ commands for utf-8 characters', function () { return __awaiter(void 0, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, client.keypress('€')];
                    case 1:
                        _a.sent();
                        expect(fetch).toHaveBeenCalledWith(clientAddr + "/keypress/Lit_%E2%82%AC", {
                            method: 'POST',
                        });
                        return [2 /*return*/];
                }
            });
        }); });
    });
    describe('#keydown()', function () {
        it('should press and hold the pause', function () { return __awaiter(void 0, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, client.keydown('Pause')];
                    case 1:
                        _a.sent();
                        expect(fetch).toHaveBeenCalledWith(clientAddr + "/keydown/Pause", {
                            method: 'POST',
                        });
                        return [2 /*return*/];
                }
            });
        }); });
    });
    describe('#keyup()', function () {
        it('should release the info button', function () {
            return client.keyup('Info').then(function () {
                expect(fetch).toHaveBeenCalledWith(clientAddr + "/keyup/Info", {
                    method: 'POST',
                });
            });
        });
    });
    describe('#icon()', function () {
        it('should download the icon to the given folder', function () {
            var response = new fetchObjects.Response(loadResponse('netflix.jpeg', true), { headers: new fetchObjects.Headers({ 'content-type': 'image/jpeg' }) });
            fetch.mockImplementation(function () { return Promise.resolve(response); });
            return client.icon('12').then(function (icon) {
                expect(icon.type).toEqual('image/jpeg');
                expect(icon.extension).toEqual('.jpeg');
                expect(icon.response).toBe(response);
            });
        });
    });
    describe('#launch()', function () {
        it('should call launch for the given app id', function () {
            return client.launch('12345').then(function () {
                expect(fetch).toHaveBeenCalledWith(client.ip + "/launch/12345", {
                    method: 'POST',
                });
            });
        });
    });
    describe('#launchDtv()', function () {
        it('should call launch/tvinput.dtv', function () { return __awaiter(void 0, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, client.launchDtv()];
                    case 1:
                        _a.sent();
                        expect(fetch).toHaveBeenCalledWith(client.ip + "/launch/tvinput.dtv", {
                            method: 'POST',
                        });
                        return [2 /*return*/];
                }
            });
        }); });
        it('should pass a channel string to launch', function () { return __awaiter(void 0, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, client.launchDtv('1.1')];
                    case 1:
                        _a.sent();
                        expect(fetch).toHaveBeenCalledWith(client.ip + "/launch/tvinput.dtv?ch=1.1", {
                            method: 'POST',
                        });
                        return [2 /*return*/];
                }
            });
        }); });
        it('should pass a channel number to launch', function () { return __awaiter(void 0, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, client.launchDtv(8.5)];
                    case 1:
                        _a.sent();
                        expect(fetch).toHaveBeenCalledWith(client.ip + "/launch/tvinput.dtv?ch=8.5", {
                            method: 'POST',
                        });
                        return [2 /*return*/];
                }
            });
        }); });
    });
    describe('#text()', function () {
        it('should send a Lit_ command for each letter', function () {
            return client.text('hello').then(function () {
                expect(fetch.mock.calls).toEqual([
                    [client.ip + "/keypress/Lit_h", { method: 'POST' }],
                    [client.ip + "/keypress/Lit_e", { method: 'POST' }],
                    [client.ip + "/keypress/Lit_l", { method: 'POST' }],
                    [client.ip + "/keypress/Lit_l", { method: 'POST' }],
                    [client.ip + "/keypress/Lit_o", { method: 'POST' }],
                ]);
            });
        });
    });
    describe('#command()', function () {
        it('should allow chaining remote commands', function () {
            return client
                .command()
                .volumeUp()
                .select()
                .text('abc')
                .send()
                .then(function () {
                expect(fetch.mock.calls).toEqual([
                    [client.ip + "/keypress/VolumeUp", { method: 'POST' }],
                    [client.ip + "/keypress/Select", { method: 'POST' }],
                    [client.ip + "/keypress/Lit_a", { method: 'POST' }],
                    [client.ip + "/keypress/Lit_b", { method: 'POST' }],
                    [client.ip + "/keypress/Lit_c", { method: 'POST' }],
                ]);
            });
        });
    });
});
//# sourceMappingURL=client.test.js.map