"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var __1 = require("..");
describe('index', function () {
    it('should export the expected interfaces', function () {
        expect(__1.RokuClient).toBeDefined();
        expect(__1.Keys).toBeDefined();
        expect(__1.Client).toBeDefined();
        expect(__1.keys).toBeDefined();
        expect(__1.discover).toBeDefined();
        expect(__1.discoverAll).toBeDefined();
    });
});
//# sourceMappingURL=index.test.js.map