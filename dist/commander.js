"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Commander = void 0;
var Keys = require("./keys");
var keyCommand_1 = require("./keyCommand");
var WAIT_COMMAND = '__WAIT';
var Commander = /** @class */ (function () {
    function Commander(client) {
        this.client = client;
        this.commands = [];
    }
    /**
     * Send the given string to the Roku device.
     * @param text The string to send.
     * @return This `Commander` instance for chaining.
     */
    Commander.prototype.text = function (text) {
        this.commands.push([text, true]);
        return this;
    };
    /**
     * Press the given key `count` times. Useful if the keys to press
     * are unknown at coding time. Each key is also available as a
     * camelcase method for convenience.
     * @param key The key to press, from the `keys` module.
     * @param count The number of times to press the key.
     */
    Commander.prototype.keypress = function (key, count) {
        if (count === void 0) { count = 1; }
        var command = keyCommand_1.getCommand(key);
        for (var i = 0; i < count; i += 1) {
            this.commands.push([command, false]);
        }
        return this;
    };
    /**
     * A convenience method for running commands conditionally. The callback
     * function will be called with the instance of the commander, and will return
     * the current commander instance. This makes it easy to run conditional logic
     * within the chain of commands.
     * @param fn A callback function that accepts the `Commander` instance. It may
     *     call any `Commander` method, or do nothing at all.
     * @return This `Commander` instance for chaining.
     *
     * @example
     * command
     *   .exec(cmd => goUp ? cmd.up(10) : cmd.down(10))
     *   .right()
     *   .select()
     *   .send();
     */
    Commander.prototype.exec = function (fn) {
        fn(this);
        return this;
    };
    /**
     * Wait the given `timeout` time before sending the next command.
     * @param timeout The number of millis to wait.
     * @return This `Commander` instance for chaining.
     */
    Commander.prototype.wait = function (timeout) {
        this.commands.push([WAIT_COMMAND, false, timeout]);
        return this;
    };
    /**
     * Send all of the configured commands to the Roku.
     */
    Commander.prototype.send = function () {
        var _this = this;
        return this.commands.reduce(function (promise, command) { return promise.then(function () { return _this.runCommand(command); }); }, Promise.resolve());
    };
    Commander.prototype.runCommand = function (_a) {
        var command = _a[0], isText = _a[1], _b = _a[2], timeout = _b === void 0 ? 0 : _b;
        if (isText) {
            return this.client.text(command);
        }
        if (command === WAIT_COMMAND) {
            return wait(timeout);
        }
        return this.client.keypress(command);
    };
    return Commander;
}());
exports.Commander = Commander;
// add all keys as methods to Commander
Object.values(Keys).forEach(function (key) {
    Commander.prototype[key.name] = function (count) {
        if (count === void 0) { count = 1; }
        return this.keypress(key.command, count);
    };
});
function wait(timeout) {
    return new Promise(function (resolve) { return setTimeout(resolve, timeout); });
}
//# sourceMappingURL=commander.js.map