import * as Keys from './keys';
import { RokuClient } from './client';
export default RokuClient;
export * from './client';
export * from './discover';
/**
 * Import `RokuClient` instead.
 * @deprecated
 */
declare const Client: typeof RokuClient;
/**
 * Import `Keys` instead.
 * @deprecated
 */
declare const keys: typeof Keys;
export { Keys, Client, keys };
