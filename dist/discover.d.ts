/**
 * Discover one Roku device on the network. Resolves to the first Roku device
 * that responds to the ssdp request.
 * @param timeout The time to wait in ms before giving up.
 * @return A promise resolving to a Roku device's address.
 */
export declare function discover(timeout?: number): Promise<string>;
/**
 * Discover all Roku devices on the network. This method always waits the full
 * timeout, resolving to a list of all Roku device addresses that responded
 * within `timeout` ms.
 * @param timeout The time to wait in ms before giving up.
 * @return A promise resolving to a list of Roku device addresses.
 */
export declare function discoverAll(timeout?: number): Promise<string[]>;
