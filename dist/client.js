"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RokuClient = exports.ROKU_DEFAULT_PORT = void 0;
var fetchPonyfill = require("fetch-ponyfill");
var camelcase = require("lodash.camelcase");
var xml2js_1 = require("xml2js");
var _debug = require("debug");
var discover_1 = require("./discover");
var commander_1 = require("./commander");
var keyCommand_1 = require("./keyCommand");
var fetch = fetchPonyfill().fetch;
var debug = _debug('roku-client:client');
/** The default port a roku device will use for remote commands. */
exports.ROKU_DEFAULT_PORT = 8060;
/**
 * Return a promise of the parsed fetch response xml.
 * @param res A fetch response object.
 */
function parseXML(res) {
    if (!res.ok) {
        throw new Error("Request failed: " + res.statusText);
    }
    return res.text().then(function (data) { return xml2js_1.parseStringPromise(data); });
}
/**
 * Convert the xml version of a roku app
 * to a cleaned up js version.
 */
function appXMLToJS(app) {
    var name = app._;
    var _a = app.$, id = _a.id, type = _a.type, version = _a.version;
    return {
        id: id,
        name: name,
        type: type,
        version: version,
    };
}
/**
 * Convert the xml version of a roku player info
 * to a cleaned up js version.
 */
function mediaPlayerXMLToJS(player) {
    var appData = player['plugin'] ? player['plugin'][0].$ : {};
    var formatData = player['format'] ? player['format'][0].$ : {};
    var bufferingData = player['buffering'] ? player['buffering'][0].$ : {};
    var newStreamData = player['new_stream'] ? player['new_stream'][0].$ : {};
    var streamSegmentData = player['stream_segment']
        ? player['stream_segment'][0].$
        : {};
    var position = player['position'] ? player['position'][0] : null;
    var duration = player['duration'] ? player['duration'][0] : null;
    var runtime = player['runtime'] ? player['runtime'][0] : null;
    var isLive = player['is_live'] ? player['is_live'][0] : null;
    var mediaPlayerInfo = {
        state: player.$['state'],
        error: player.$['error'],
        app: appData,
        format: formatData,
        position: position,
        runtime: runtime,
        duration: duration,
        isLive: isLive,
        buffering: bufferingData,
        newStream: newStreamData,
        streamSegment: streamSegmentData,
    };
    return mediaPlayerInfo;
}
/**
 * The Roku client class. Contains methods to talk to a roku device.
 */
var RokuClient = /** @class */ (function () {
    /**
     * Construct a new `Client` object with the given address.
     * @param ip The address of the Roku device on the network. If no port is
     *     given, then the default roku remote port will be used.
     */
    function RokuClient(ip) {
        this.ip = ip;
        if (!ip.startsWith('http://')) {
            ip = "http://" + ip;
        }
        // no port at end
        if (!/:\d+$/.test(ip)) {
            ip = ip + ":" + exports.ROKU_DEFAULT_PORT;
        }
        this.ip = ip;
    }
    /**
     * Return a promise resolving to a new `Client` object for the first Roku
     * device discovered on the network. This method resolves to a single
     * `Client` object.
     * @param timeout The time in ms to wait before giving up.
     * @return A promise resolving to a `Client` object.
     */
    RokuClient.discover = function (timeout) {
        return discover_1.discover(timeout).then(function (ip) { return new RokuClient(ip); });
    };
    /**
     * Return a promise resolving to a list of `Client` objects corresponding to
     * each roku device found on the network. Check the client's ip member to see
     * which device the client corresponds to.
     * @param timeout The time in ms to wait before giving up.
     * @return A promise resolving to a list of `Client` objects.
     */
    RokuClient.discoverAll = function (timeout) {
        return discover_1.discoverAll(timeout).then(function (ips) {
            return ips.map(function (ip) { return new RokuClient(ip); });
        });
    };
    /**
     * Get a list of apps installed on this device.
     * @see {@link https://developer.roku.com/docs/developer-program/debugging/external-control-api.md#queryapps-example}
     */
    RokuClient.prototype.apps = function () {
        var endpoint = this.ip + "/query/apps";
        debug("GET " + endpoint);
        return fetch(endpoint)
            .then(parseXML)
            .then(function (_a) {
            var apps = _a.apps;
            return apps.app.map(appXMLToJS);
        });
    };
    /**
     * Get the active app, or null if the home screen is displayed.
     * @see {@link https://developer.roku.com/docs/developer-program/debugging/external-control-api.md#queryactive-app-examples}
     */
    RokuClient.prototype.active = function () {
        var endpoint = this.ip + "/query/active-app";
        debug("GET " + endpoint);
        return fetch(endpoint)
            .then(parseXML)
            .then(function (data) {
            var app = data['active-app'].app;
            if (app.length !== 1) {
                throw new Error("expected 1 active app but received " + app.length + ": " + app);
            }
            var activeApp = app[0];
            // If no app is currently active, a single field is returned without
            // any properties
            if (!activeApp.$ || !activeApp.$.id) {
                return null;
            }
            return appXMLToJS(activeApp);
        });
    };
    /**
     * Get the info of this Roku device. Responses vary between devices.
     * All keys are coerced to camelcase for easier access, so user-device-name
     * becomes userDeviceName, etc.
     * @see {@link https://developer.roku.com/docs/developer-program/debugging/external-control-api.md#querydevice-info-example}
     */
    RokuClient.prototype.info = function () {
        var endpoint = this.ip + "/query/device-info";
        debug("GET " + endpoint);
        return fetch(endpoint)
            .then(parseXML)
            .then(function (data) {
            return Object.entries(data['device-info']).reduce(
            // the xml parser wraps values in an array
            function (result, _a) {
                var key = _a[0], value = _a[1][0];
                result[camelcase(key)] = value;
                return result;
            }, {});
        });
    };
    /**
     * Get the active app, or null if the home screen is displayed.
     * @see {@link https://developer.roku.com/docs/developer-program/debugging/external-control-api.md#queryactive-app-examples}
     */
    RokuClient.prototype.mediaPlayer = function () {
        var endpoint = this.ip + "/query/media-player";
        debug("GET " + endpoint);
        return fetch(endpoint)
            .then(parseXML)
            .then(function (data) {
            var player = data['player'];
            if (!player) {
                return null;
            }
            return mediaPlayerXMLToJS(player);
        });
    };
    /**
     * Fetch the given icon from the Roku device and return an object containing
     * the image type, extension, and the fetch response. The response can be
     * streamed to a file, turned into a data url, etc.
     * @see {@link https://developer.roku.com/docs/developer-program/debugging/external-control-api.md#queryicon-example}
     * @param appId The app id to get the icon of.
     *     Should be the id from the id field of the app.
     * @return An object containing the fetch response.
     */
    RokuClient.prototype.icon = function (appId) {
        var endpoint = this.ip + "/query/icon/" + appId;
        debug("GET " + endpoint);
        return fetch(endpoint).then(function (response) {
            if (!response.ok) {
                throw new Error("Failed to fetch icon for app " + appId + ": " + response.statusText);
            }
            var type = response.headers.get('content-type') || undefined;
            var extension = undefined;
            if (type) {
                var match = /image\/(.*)/.exec(type);
                if (match) {
                    extension = "." + match[1];
                }
            }
            return { type: type, extension: extension, response: response };
        });
    };
    /**
     * Launch the given `appId`.
     * @see {@link https://developer.roku.com/docs/developer-program/debugging/external-control-api.md#launch-examples}
     * @param appId The id of the app to launch.
     * @return A void promise which resolves when the app is launched.
     */
    RokuClient.prototype.launch = function (appId) {
        var endpoint = this.ip + "/launch/" + appId;
        debug("POST " + endpoint);
        return fetch(endpoint, { method: 'POST' }).then(function (res) {
            if (!res.ok) {
                throw new Error("Failed to call " + endpoint + ": " + res.statusText);
            }
        });
    };
    /**
     * Launch the DTV tuner, optionally with a channel number.
     * @see {@link https://developer.roku.com/docs/developer-program/debugging/external-control-api.md#launch-parameters-for-the-roku-tv-tuner-app-channel-id-tvinputdtv}
     * @param channel The channel to launch, or leave blank to launch the DTV
     *     ui to the last open channel.
     * @return A promise which resolves when DTV is launched.
     */
    RokuClient.prototype.launchDtv = function (channel) {
        var channelQuery = channel ? "?ch=" + channel : '';
        var appId = "tvinput.dtv" + channelQuery;
        return this.launch(appId);
    };
    /**
     * Helper used by all keypress methods. Converts single characters
     * to `Lit_` commands to send the letter to the Roku.
     * @see {@link https://developer.roku.com/docs/developer-program/debugging/external-control-api.md#keypress-key-values}
     * @param func The name of the Roku endpoint function.
     * @param key The key to press.
     */
    RokuClient.prototype.keyhelper = function (func, key) {
        var command = keyCommand_1.getCommand(key);
        // if a single key is sent, treat it as a letter
        var keyCmd = command.length === 1 ? "Lit_" + encodeURIComponent(command) : command;
        var endpoint = this.ip + "/" + func + "/" + keyCmd;
        debug("POST " + endpoint);
        return fetch(endpoint, { method: 'POST' }).then(function (res) {
            if (!res.ok) {
                throw new Error("Failed to call " + endpoint + ": " + res.statusText);
            }
        });
    };
    /**
     * Equivalent to pressing and releasing the remote control key given.
     * @see {@link https://developer.roku.com/docs/developer-program/debugging/external-control-api.md#keypress-example}
     * @param key A key from the keys module.
     * @return A promise which resolves when the keypress has completed.
     */
    RokuClient.prototype.keypress = function (key) {
        return this.keyhelper('keypress', key);
    };
    /**
     * Equivalent to pressing and holding the remote control key given.
     * @see {@link https://developer.roku.com/docs/developer-program/debugging/external-control-api.md#keyupkeydown-example}
     * @param key A key from the keys module.
     * @return A promise which resolves when the keydown has completed.
     */
    RokuClient.prototype.keydown = function (key) {
        return this.keyhelper('keydown', key);
    };
    /**
     * Equivalent to releasing the remote control key given. Only makes sense
     * if `keydown` was already called for the same key.
     * @see {@link https://developer.roku.com/docs/developer-program/debugging/external-control-api.md#keyupkeydown-example}
     * @param key A key from the keys module.
     * @return A promise which resolves when the keyup has completed.
     */
    RokuClient.prototype.keyup = function (key) {
        return this.keyhelper('keyup', key);
    };
    /**
     * Send the given string to the Roku device.
     * A shorthand for calling `keypress` for each letter in the given string.
     * @see {@link https://developer.roku.com/docs/developer-program/debugging/external-control-api.md#keypress-key-values}
     * @param text The message to send.
     * @return A promise which resolves when the text has successfully been sent.
     */
    RokuClient.prototype.text = function (text) {
        var _this = this;
        return text
            .split('')
            .reduce(function (promise, letter) { return promise.then(function () { return _this.keypress(letter); }); }, Promise.resolve());
    };
    /**
     * Chain multiple remote commands together in one convenient api.
     * Each value in the `keys` module is available as a command in
     * camelcase form, and can take an optional number to indicate how many
     * times the button should be pressed. A `text` method is also available
     * to send a full string. After composing the command, `send` should
     * be called to perform the scripted commands. The result of calling
     * `.command()` can be stored in a variable and modified before calling send.
     *
     * @example
     * client.command()
     *   .volumeUp(10)
     *   .up(2)
     *   .select()
     *   .text('Breaking Bad')
     *   .enter()
     *   .send();
     *
     * @return A commander instance.
     */
    RokuClient.prototype.command = function () {
        return new commander_1.Commander(this);
    };
    return RokuClient;
}());
exports.RokuClient = RokuClient;
//# sourceMappingURL=client.js.map