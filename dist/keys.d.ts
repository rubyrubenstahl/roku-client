/**
 * Create a mapping of keys to make them easier to remember.
 * @see https://sdkdocs.roku.com/display/sdkdoc/External+Control+Guide#ExternalControlGuide-KeypressKeyValues
 */
export declare const HOME: {
    command: "Home";
    name: "home";
};
export declare const REV: {
    command: "Rev";
    name: "reverse";
};
export declare const REVERSE: {
    command: "Rev";
    name: "reverse";
};
export declare const FWD: {
    command: "Fwd";
    name: "forward";
};
export declare const FORWARD: {
    command: "Fwd";
    name: "forward";
};
export declare const PLAY: {
    command: "Play";
    name: "play";
};
export declare const SELECT: {
    command: "Select";
    name: "select";
};
export declare const LEFT: {
    command: "Left";
    name: "left";
};
export declare const RIGHT: {
    command: "Right";
    name: "right";
};
export declare const DOWN: {
    command: "Down";
    name: "down";
};
export declare const UP: {
    command: "Up";
    name: "up";
};
export declare const BACK: {
    command: "Back";
    name: "back";
};
export declare const INSTANT_REPLAY: {
    command: "InstantReplay";
    name: "instantReplay";
};
export declare const INFO: {
    command: "Info";
    name: "info";
};
export declare const STAR: {
    command: "Info";
    name: "star";
};
export declare const OPTIONS: {
    command: "Info";
    name: "options";
};
export declare const BACKSPACE: {
    command: "Backspace";
    name: "backspace";
};
export declare const SEARCH: {
    command: "Search";
    name: "search";
};
export declare const ENTER: {
    command: "Enter";
    name: "enter";
};
export declare const FIND_REMOTE: {
    command: "FindRemote";
    name: "findRemote";
};
export declare const VOLUME_DOWN: {
    command: "VolumeDown";
    name: "volumeDown";
};
export declare const VOLUME_UP: {
    command: "VolumeUp";
    name: "volumeUp";
};
export declare const VOLUME_MUTE: {
    command: "VolumeMute";
    name: "volumeMute";
};
export declare const CHANNEL_UP: {
    command: "ChannelUp";
    name: "channelUp";
};
export declare const CHANNEL_DOWN: {
    command: "ChannelDown";
    name: "channelDown";
};
export declare const INPUT_TUNER: {
    command: "InputTuner";
    name: "inputTuner";
};
export declare const INPUT_HDMI1: {
    command: "InputHDMI1";
    name: "inputHDMI1";
};
export declare const INPUT_HDMI2: {
    command: "InputHDMI2";
    name: "inputHDMI2";
};
export declare const INPUT_HDMI3: {
    command: "InputHDMI3";
    name: "inputHDMI3";
};
export declare const INPUT_HDMI4: {
    command: "InputHDMI4";
    name: "inputHDMI4";
};
export declare const INPUT_AV1: {
    command: "InputAV1";
    name: "inputAV1";
};
export declare const POWER: {
    command: "Power";
    name: "power";
};
